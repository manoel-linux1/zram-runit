# ZRAM-RUNIT is no longer being maintained

- zram-runit-version: sep 2023

- build-latest: 0.0.1

- Support for the distro: Void-Linux/Artix-with-runit

- ZRAM-RUNIT is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with ZRAM-RUNIT, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of ZRAM-RUNIT to add additional features.

- systemd version, access the link: https://gitlab.com/manoel-linux1/zram-systemd if you have a distro with systemd.

## Installation

- To install ZRAM-RUNIT, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/zram-runit.git

# 2. To install the ZRAM-RUNIT script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# For check version

sudo `zram-runit-version` or `zram-runit-version`

# For change the zram-runit configuration

sudo `zram-runit-change`

# For stop the zram-runit

sudo `zram-runit-stop`

# For start the zram-runit

sudo `zram-runit-start`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The ZRAM-RUNIT project is currently in development. The latest stable version is 0.0.1. We aim to provide regular updates and add more features in the future.

# License

- ZRAM-RUNIT is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of ZRAM-RUNIT.