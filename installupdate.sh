#!/bin/bash

clear

show_main_menu() {
while true; do
clear
echo "#################################################################"
echo "(zram-runit-installer) >> (sep 2023)"
echo "#################################################################"
echo " ███████ ██████   █████  ███    ███ "
echo "    ███  ██   ██ ██   ██ ████  ████ "
echo "   ███   ██████  ███████ ██ ████ ██ "
echo "  ███    ██   ██ ██   ██ ██  ██  ██ "
echo " ███████ ██   ██ ██   ██ ██      ██ "    
echo "#################################################################"
echo "(zram-runit-gitlab) >> (https://gitlab.com/manoel-linux1/zram-runit)"
echo "#################################################################"

if [[ $EUID -ne 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(Superuser privileges or sudo required to execute the script)" 
echo "#################################################################"
exit 1
fi

sudo pacman -Sy
sudo pacman -S iputils -y
echo "#################################################################"
sudo xbps-install -Sy
sudo xbps-install -S inetutils-ping -y
echo "#################################################################"

clear

echo "#################################################################"
echo "(1)> (Install) >> (the ZRAM-RUNIT version of Void-Linux)"
echo "(2)> (Install) >> (the ZRAM-RUNIT version of Artix with runit)"
echo "(3)> (Exit)"
echo "#################################################################"

read -p "(Enter your choice) >> " choice
echo "#################################################################"

case $choice in
1)
show_void-linux
;;
2)
show_artix-with-runit
;;
3)
exit 0
;;
*)
echo "(Invalid choice. Please try again)"
echo "#################################################################"
sleep 2
;;
esac
done
}

show_void-linux() {
while true; do
clear
if [ ! -x /bin/xbps-install ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "(Checking for updates in Void Linux)" 
echo "#################################################################"
sudo xbps-install -Sy
sudo xbps-install -S unzip binutils tar curl xbps xz grep gawk sed -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo xbps-install -Sy
sudo xbps-install -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

sudo rm -rf /usr/bin/zram-runit-gitlab

sudo mkdir /usr/bin/zram-runit-gitlab

sudo cp runit/run /usr/bin/zram-runit-gitlab

clear

echo "#################################################################"
read -p "(Enter the desired size for ZRAM (e.g., 2G or 4G or 8G) >> " zram_size_input
echo "#################################################################"

if [[ $zram_size_input == *GB ]]; then
echo "#################################################################"
echo "(Please enter the size with 'G' in uppercase, for example >> 2G or 4G or 8G)"
echo "#################################################################"
exit 1
fi

if [[ $zram_size_input == *gb ]]; then
echo "#################################################################"
echo "(Please enter the size with 'G' in uppercase, for example >> 2G or 4G or 8G)"
echo "#################################################################"
exit 1
fi

if [[ $zram_size_input == *g ]]; then
echo "#################################################################"
echo "(Please enter the size with 'G' in uppercase, for example >> 2G or 4G or 8G)"
echo "#################################################################"
exit 1
fi

zram_size="${zram_size_input//G}"
read -p "(Choose a compression algorithm (l for lz4, z for zstd) >> " selected_algorithm
echo "#################################################################"
case $selected_algorithm in
l)
compression_algorithm="lz4"
;;
z)
compression_algorithm="zstd"
;;
*)
echo "(Invalid choice. Using the default algorithm (lz4)"
echo "#################################################################"
compression_algorithm="lz4"
;;
esac

read -p "(Enter the desired priority for ZRAM (e.g., 100 or 200) >> " zram_priority
echo "#################################################################"

if [[ $zram_priority == "10%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "20%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "30%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "40%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "50%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "60%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "60%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "70%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "80%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "90%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "100%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "110%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "120%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "130%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "140%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "150%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "160%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "170%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "180%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "190%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "200%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "210%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "220%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "230%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "240%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "250%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "260%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "270%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "280%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "290%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "300%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "310%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "320%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "330%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "340%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "350%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "360%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "300%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "370%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "380%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "390%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "400%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "410%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "420%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "430%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "440%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "450%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "460%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "470%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "480%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "490%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "500%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

echo "#!/bin/sh

echo 0 > /sys/module/zswap/parameters/enabled
modprobe zram
zramctl /dev/zram0 --algorithm $compression_algorithm --size ${zram_size}G
mkswap /dev/zram0
swapon --priority $zram_priority /dev/zram0" > runit/run

sudo sv stop zram-runit-gitlab

sudo rm /var/service/zram-runit-gitlab

sudo sv stop zram-runit-gitlab

sudo rm /var/service/zram-runit-gitlab

sudo swapoff /dev/zram0

sudo modprobe -r zram

sudo echo 1 > /sys/module/zswap/parameters/enabled

sudo rm -rf /usr/bin/zram-runit-version

sudo rm -rf /usr/bin/zram-runit-change

sudo rm -rf /usr/bin/zram-runit-start

sudo rm -rf /usr/bin/zram-runit-stop

sudo rm -rf /etc/sv/zram-runit-gitlab

sudo mkdir -p /etc/sv/zram-runit-gitlab

sudo cp runit/run /etc/sv/zram-runit-gitlab

sudo cp runit/zram-runit-version /usr/bin/

sudo cp runit/zram-runit-change /usr/bin/

sudo cp runit/zram-runit-start /usr/bin/

sudo cp runit/zram-runit-stop /usr/bin/

sudo chmod +x /etc/sv/zram-runit-gitlab/run

sudo chmod +x /usr/bin/zram-runit-version

sudo chmod +x /usr/bin/zram-runit-change

sudo chmod +x /usr/bin/zram-runit-start

sudo chmod +x /usr/bin/zram-runit-stop

sudo ln -s /etc/sv/zram-runit-gitlab /var/service/

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(To change the zram-runit configuration, use the command >> sudo zram-runit-change)"
echo "(To stop the zram-runit, use the command >> sudo zram-runit-stop)"
echo "(To start the zram-runit, use the command >> sudo zram-runit-start)"
echo "(To check the version of zram-runit, use the command >> sudo zram-runit-version or zram-runit-version)"
echo "#################################################################"  
read -p "(To apply the changes, you need to restart system) (y/n) >> " confirm
                
if [[ "$confirm" == "y" || "$confirm" == "Y" ]]; then
echo "#################################################################"
echo "(Restarting the system)"    
echo "#################################################################"
sudo reboot
else
echo "#################################################################"
echo "(Restart canceled)"
echo "#################################################################"
fi
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_artix-with-runit() {
while true; do
clear
if [ ! -x /bin/pacman ]; then
echo "#################################################################"
echo "(Warning) >> (You are trying to run a version meant for another distribution) 
(To prevent issues, the script has blocked a warning to execute the version meant for your distribution)"
echo "#################################################################"
exit 1
fi
echo "#################################################################"
echo "(Checking for updates in Artix)" 
echo "#################################################################"
sudo pacman -Sy
sudo pacman -S unzip binutils tar curl xz grep gawk sed -y
clear
echo "#################################################################"

read -p "(Do you want to update your system) (y/n) >> " choice
echo "#################################################################"
if [[ $choice == "y" || $choice == "Y" ]]; then
sudo pacman -Sy
sudo pacman -Syu -y
else
echo "(Skipping system update)"
echo "#################################################################"
fi

clear

sudo rm -rf /usr/bin/zram-runit-gitlab

sudo mkdir /usr/bin/zram-runit-gitlab

sudo cp runit/run /usr/bin/zram-runit-gitlab

clear

echo "#################################################################"
read -p "(Enter the desired size for ZRAM (e.g., 2G or 4G or 8G) >> " zram_size_input
echo "#################################################################"

if [[ $zram_size_input == *GB ]]; then
echo "#################################################################"
echo "(Please enter the size with 'G' in uppercase, for example >> 2G or 4G or 8G)"
echo "#################################################################"
exit 1
fi

if [[ $zram_size_input == *gb ]]; then
echo "#################################################################"
echo "(Please enter the size with 'G' in uppercase, for example >> 2G or 4G or 8G)"
echo "#################################################################"
exit 1
fi

if [[ $zram_size_input == *g ]]; then
echo "#################################################################"
echo "(Please enter the size with 'G' in uppercase, for example >> 2G or 4G or 8G)"
echo "#################################################################"
exit 1
fi

zram_size="${zram_size_input//G}"
read -p "(Choose a compression algorithm (l for lz4, z for zstd) >> " selected_algorithm
echo "#################################################################"
case $selected_algorithm in
l)
compression_algorithm="lz4"
;;
z)
compression_algorithm="zstd"
;;
*)
echo "(Invalid choice. Using the default algorithm (lz4)"
echo "#################################################################"
compression_algorithm="lz4"
;;
esac

read -p "(Enter the desired priority for ZRAM (e.g., 100 or 200) >> " zram_priority
echo "#################################################################"

if [[ $zram_priority == "10%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "20%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "30%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "40%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "50%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "60%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "60%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "70%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "80%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "90%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "100%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "110%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "120%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "130%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "140%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "150%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "160%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "170%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "180%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "190%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "200%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "210%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "220%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "230%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "240%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "250%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "260%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "270%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "280%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "290%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "300%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "310%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "320%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "330%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "340%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "350%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "360%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "300%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "370%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "380%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "390%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "400%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "410%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "420%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "430%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "440%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "450%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "460%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "470%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "480%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "490%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "500%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

if [[ $zram_priority == "%" ]]; then
zram_priority="100"
echo "(Automatic priority (100) chosen)"
echo "#################################################################"
fi

echo "#!/bin/sh

echo 0 > /sys/module/zswap/parameters/enabled
modprobe zram
zramctl /dev/zram0 --algorithm $compression_algorithm --size ${zram_size}G
mkswap /dev/zram0
swapon --priority $zram_priority /dev/zram0" > runit/run

sudo sv stop zram-runit-gitlab

sudo rm /run/runit/service/zram-runit-gitlab

sudo sv stop zram-runit-gitlab

sudo rm /run/runit/service/zram-runit-gitlab

sudo swapoff /dev/zram0

sudo modprobe -r zram

sudo echo 1 > /sys/module/zswap/parameters/enabled

sudo rm -rf /usr/bin/zram-runit-version

sudo rm -rf /usr/bin/zram-runit-change

sudo rm -rf /usr/bin/zram-runit-start

sudo rm -rf /usr/bin/zram-runit-stop

sudo rm -rf /etc/runit/sv/zram-runit-gitlab

sudo mkdir -p /etc/runit/sv/zram-runit-gitlab

sudo cp runit/run /etc/runit/sv/zram-runit-gitlab

sudo cp runit/zram-runit-version /usr/bin/

sudo cp runit/zram-runit-change /usr/bin/

sudo cp runit/zram-runit-start /usr/bin/

sudo cp runit/zram-runit-stop /usr/bin/

sudo chmod +x /etc/runit/sv/zram-runit-gitlab/run

sudo chmod +x /usr/bin/zram-runit-version

sudo chmod +x /usr/bin/zram-runit-change

sudo chmod +x /usr/bin/zram-runit-start

sudo chmod +x /usr/bin/zram-runit-stop

sudo ln -s /etc/runit/sv/zram-runit-gitlab /run/runit/service/

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"
echo "(Installation/Update completed)"
echo "#################################################################"
echo "(To change the zram-runit configuration, use the command >> sudo zram-runit-change)"
echo "(To stop the zram-runit, use the command >> sudo zram-runit-stop)"
echo "(To start the zram-runit, use the command >> sudo zram-runit-start)"
echo "(To check the version of zram-runit, use the command >> sudo zram-runit-version or zram-runit-version)"
echo "#################################################################"  
read -p "(To apply the changes, you need to restart system) (y/n) >> " confirm
                
if [[ "$confirm" == "y" || "$confirm" == "Y" ]]; then
echo "#################################################################"
echo "(Restarting the system)"    
echo "#################################################################"
sudo reboot
else
echo "#################################################################"
echo "(Restart canceled)"
echo "#################################################################"
fi
read -rsn1 -p "(press Enter to return to the main menu)
#################################################################" key
if [[ $key == "r" || $key == "R" ]]; then
continue
fi

break
done

echo "#################################################################"
}

show_main_menu
