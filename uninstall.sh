#!/bin/bash

clear
echo "#############################################################"
echo "(zram-runit-uninstaller) >> (sep 2023)"
echo "#############################################################"
echo " ███████ ██████   █████  ███    ███ "
echo "    ███  ██   ██ ██   ██ ████  ████ "
echo "   ███   ██████  ███████ ██ ████ ██ "
echo "  ███    ██   ██ ██   ██ ██  ██  ██ "
echo " ███████ ██   ██ ██   ██ ██      ██ "  
echo "#############################################################"
echo "(zram-runit-gitlab) >> (https://gitlab.com/manoel-linux1/zram-runit)"
echo "#############################################################"

if [[ $EUID -ne 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#############################################################"
echo "(Superuser privileges or sudo required to execute the script)" 
echo "#############################################################"
exit 1
fi

clear

sudo sv stop zram-runit-gitlab

sudo rm /var/service/zram-runit-gitlab

sudo sv stop zram-runit-gitlab

sudo rm /var/service/zram-runit-gitlab

sudo sv stop zram-runit-gitlab

sudo rm /run/runit/service/zram-runit-gitlab

sudo sv stop zram-runit-gitlab

sudo rm /run/runit/service/zram-runit-gitlab

sudo swapoff /dev/zram0

sudo modprobe -r zram

sudo echo 1 > /sys/module/zswap/parameters/enabled

sudo rm -rf /usr/bin/zram-runit-gitlab

sudo rm /usr/bin/zram-runit-version

sudo rm /usr/bin/zram-runit-change

sudo rm /usr/bin/zram-runit-start

sudo rm /usr/bin/zram-runit-stop

sudo rm -rf /etc/sv/zram-runit-gitlab

sudo rm -rf /etc/runit/sv/zram-runit-gitlab

clear

echo "#############################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ " 
echo "#############################################################"
echo "(Uninstallation completed)"
echo "#############################################################"